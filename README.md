# Yii2 extension that allows to create backups of mysql database (structure and data) 

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist fadeevms/yii2-dump-db "*"
```

or add

```json
"fadeevms/yii2-dump-db": "*"
```

to the require section of your composer.json.


To use this extension,  simply add the following code in your application configuration:

```php
    'components' => [
        ...
        'dumper' => [
            'class' => 'fadeevms\dump\dumpDB',
        ],
        ...
    ],
```

Usage:

          $dumper = \Yii::$app->dumper;
          echo $dumper->getDump();

Saving the dump to a file:

          $dumper = \Yii::$app->dumper;
          $bk_file = 'FILE_NAME-'.date('YmdHis').'.sql';
          $fh = fopen($bk_file, 'w') or die("can't open file");
          fwrite($fh, $dumper->getDump(FALSE));
          fclose($fh);

Dumping external DB:

         $dumper = new \fadeevms\dump\dumpDB('mysql:host=HOTS_NAME_OR_IP;dbname=DATABASE_NAME','USERNAME','PASSWORD');
         $dumper->setRemoveViewDefinerSecurity(TRUE);
         echo $dumper->getDump();

